<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('info');
});

Route::get('/item/{itemnnumber}/{name}/{price}', function ($itemnumber,$name,$price) {
    return "Item Number: ".$itemnumber." Name: ".$name." Price: ".$price;
});

Route::get('/order/{custnumber}/{name}/{ordernumber}/{date}', function ($custnumber,$name,$ordernumber,$date) {
    return "Customer No: ".$custnumber." Name: ".$name." Order No: ".$ordernumber." Date: ".$date;
});

Route::get('/orderdetails/{transnumber}/{ordernumber}/{itemid}/{name}/{price}/{quantity}', 
    function ($transnumber,$ordernumber,$itemid,$name,$price,$quantity) {
    return "Trans No: ".$transnumber." Order No: ".$ordernumber." Item ID: ".$itemid." Name: ".$name." Price: ".$price." Quantity: ".$price*$quantity;
});

Route::get('/customer/{custnumber}/{name}/{address}/{age?}', function ($custnumber,$name,$address,$age="Undefined") {
    return "Customer No: ".$custnumber." Name: ".$name." Address: ".$address." Age: ".$age;
});