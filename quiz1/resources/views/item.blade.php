<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Item</title>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                text-align: center;
            }
            .info 
            {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
            <div class="info">
                <h1>ITEM</h1>
                <br>
                <h1>Catabay Louie</h1>
                <h2>BSIT-3C</h2>
                <a href="/order">ORDER</a>
            </div>
    </body>
</html>
