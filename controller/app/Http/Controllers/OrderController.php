<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
	    public function DisplayCustomer($customerid,$name,$address)
    {
    	return view('customer')->with('customerid',$customerid)->with('name',$name)->with('address',$address);
    }

    public function DisplayItem($itemnumber,$name,$price)
    {
    	return view('item')->with('itemnumber',$itemnumber)->with('name',$name)->with('price',$price);
    }

    public function DisplayOrder($customerid,$name,$ordernumber,$date)
    {
    	return view('order')->with('customerid',$customerid)->with('name',$name)->with('ordernumber',$ordernumber)
    	->with('date',$date);
    }

    public function DisplayOrderDetails($transnumber,$ordernumber,$itemid,$name,$price,$quantity)
    {
    	return view('orderdetails')->with('transnumber',$transnumber)->with('ordernumber',$ordernumber)
    	->with('itemid',$itemid)->with('name',$name)->with('price',$price)->with('quantity',$quantity);
    }    

}
