<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/item', function () {
    return view('item');
});*/

Route::get('/customer/{customerid}/{name}/{address}','App\Http\Controllers\OrderController@DisplayCustomer');
Route::get('/item/{itemnumber}/{name}/{price}','App\Http\Controllers\OrderController@DisplayItem');
Route::get('/order/{customerid}/{name}/{ordernumber}/{date}','App\Http\Controllers\OrderController@DisplayOrder');
Route::get('/orderdetails/{itransnumber}/{ordernumber}/{itemid}/{name}/{price}/{quantity}',
    'App\Http\Controllers\OrderController@DisplayOrderDetails');
