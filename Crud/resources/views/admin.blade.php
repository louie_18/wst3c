
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

	<title>Admin</title>
<style>
.container
{
	margin-top: 50px;
}
</style>

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-13">
            <div class="card">
                <div class="card-header">
                    <h4><strong>Dr. Mundo Scheduled Appointments</strong>
                        <a href="{{ route('login')}}" class="btn btn-danger float-end">Log in</a>
                    </h4>
                </div>
                
                <div class="card-body">
                    <table class="table table-hover">
                    	<thead>
                    		<th>Time</th>
                    		<th>Date</th>
                    	</thead>
                    	<tbody>

							@if(Session::has('success'))
							<div class="alert alert-success">{{Session::get('success')}}</div>
							@endif	

                    		@foreach($data as $timedate)
                    		<tr>

                    		<td>
                    			{{$timedate->time}}
                    		</td>

                    		<td>
                    			{{$timedate->date}}
                    		</td>

                    		</tr>
                    		@endforeach

                    	</tbody>
                    </table>
                </div>

            </div>
            
        </div>
        
    </div>
    
</div>


</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</html>