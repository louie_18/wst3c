
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<title>Dashboard</title>
<style>
.container
{
	margin-top: 50px;
}
</style>

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-13">
            <div class="card">
                <div class="card-header">
                    <h4><strong>Set an appointment to the dentist</strong>
                        <a href="{{ route('login')}}" class="btn btn-danger float-end">Log out</a>
                    </h4>
                </div>
                
                <div class="card-body">
                    <table class="table table-hover">
                    	<thead>
                    		<th>Available</th>
                    		<th>Time</th>
                    		<th>Date</th>
                    		<th>Book</th>
                    	</thead>
                    	<tbody>
                    		<td>Dr.Mundo</td>
                    		<form action="{{ route('apt')}}" method="post">
                    			
							@if(Session::has('success'))
							<div class="alert alert-success">{{Session::get('success')}}</div>
							@endif

							@if(Session::has('fail'))
							<div class="alert alert-danger">{{Session::get('fail')}}</div>
							@endif

                    		<td>
								{{csrf_field()}}
                    			<div class="form-group" >
								<select name="time" class="form-control" required>
									<option>8:00 AM - 10:00 AM</option>
									<option>10:00 AM - 12:00 PM</option>
									<option>1:00 PM - 3:00 PM</option>
									<option>3:00 PM - 5:00 PM</option>
								</select>
								</div>
                    		</td>
                    		<td><input type="text" name="dt" class="form-control datepicker" required></td>

                    		<td><button type="submit" class="btn btn-block btn-success">Submit</button></td>
                    		</form>
                    	</tbody>
                    </table>
                </div>

            </div>
            
        </div>
        
    </div>
    
</div>


</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<script>
   
    $('.datepicker').datepicker({ 
        startDate: new Date()
    });
  
</script>
</html>