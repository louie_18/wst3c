<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Session;
use Hash;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    
    function login()
    {
        return  view ('login');
    }
    function register()
    {
        return view ('register');
    }
    function registeruser(Request $request)
    {
        //echo "value posted";
        $request->validate([
            'username'=>'required|min:3|max:12',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:8|max:12'
        ]);

        $user = new Users();
        $user->name=$request->username;
        $user->email=$request->email;
        $user->password=$request->password;
        $res=$user->save();

        if ($res) 
        {
            // code...
            return back()->with('success','Registration complete');
        }
        else 
        {
            // code...
            return back()->with('fail','Registration failed');
        }
    }

    function loginuser(Request $request)
    {
        $request->validate([
            'username'=>'required|min:3|max:12',
            'password'=>'required|min:8|max:12'
        ]);

        $user=DB::table('users')->where('name',$request->username)->where('password',$request->password)->first();

        if ($user) 
        {
            // code...
                $id=DB::table('users')->where('name',$request->username)->pluck('id');
                return view('dashboard',$id);

        }
        else 
        {
            // code...
            return back()->with('fail','Invalid Login Credentials');
        }

    }

    function loggedinuser()
    {

        return view('dashboard');

    }


    function addappoint(Request $request)
    {

         $sched=DB::table('apt')->where('time',$request->time)->where('date',$request->dt)->first();
        if ($sched)
        {
           return back()->with('fail','Schedule Already Taken');
        }
        else
        {
            $add=DB::table('apt')->insert([
                'time'=>$request->time,
                'date'=>$request->dt
            ]);
           return back()->with('success','Appointment Scheduled');
        }
    }

    function existappoint()
    {
        return view('dashboard');
    }

    function admin(Request $request)
    {

        $timedata=array(
            'data'=>DB::table('apt')->get()
        );
        return view('admin',$timedata);
    }

    // function delete($id)
    // {
    //     DB::table('apt')->where('id',$id)->delete();
    //     return back()->with('success','Appointment Attended');
    // }

    // function deleted()
    // {
    //     return view('admin');
    // }
}
