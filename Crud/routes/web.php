<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//login
Route::get('/login', [UserController::class,'login'])->name('login');
Route::get('/register', [UserController::class,'register']);
Route::post('/register', [UserController::class,'registeruser'])->name('registeruser');
Route::post('/loginuser/book', [UserController::class,'loginuser'])->name('loginuser');
Route::get('/loginuser/book', [UserController::class,'loggedinuser']);

//appointment
Route::post('appointments', [UserController::class,'addappoint'])->name('apt');
Route::get('appointments', [UserController::class,'existappoint']);

//admin page
Route::get('/login/appointments', [UserController::class,'admin'])->name('admin');
