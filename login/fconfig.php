<?php 
define('APP_ID','1241815536343106');
define('APP_SECRET', 'f942bc713b93ce152ab46469d444b194');
define('API_VERSION', 'v2.5');
define('FB_BASE_URL', 'http://localhost/login/fhome.php');

if (!session_id()) 
{
session_start();	
}
require_once(__DIR__.'/Facebook/autoload.php');
$fb = new Facebook\Facebook([
'app_id'=>APP_ID,
'app_secret'=>APP_SECRET,
'default_graph_version'=>API_VERSION,
]);

$fb_helper=$fb->getRedirectLoginHelper();

try
{
	if (isset($_SESSION['facebook_access_token'])) 
	{
		$access_Token=$_SESSION['facebook_access_token'];
	}
	else
	{
		$access_Token=$fb_helper->getAccessToken();
	}
}
catch (FacebookResponseException$e)
{
	echo 'Facebook API error: '. $e->getMessage();
	exit;
}
catch(FacebookSDKException $e)
{
	echo 'Facebook SDK Error: '. $e->getMessage();
	exit;
}

?>