<?php
		require_once "gconfig.php";
		$loginURL = $gClient->createAuthUrl();
?>

<?php 

require_once "fconfig.php";

if (isset($accessToken)) 
{
	if (!isset($_SESSION['facebook_access_token'])) 
	{
		$_SESSION['facebook_access_token']=(string)$accessToken;

		$oAuth2Client=$fb->getOAuth2Client();

		$longLivedAccessToken=$oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token']=(string)$longLivedAccessToken;

		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	else
	{
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}

	try
	{
		$fb_response=$fb->get('/me?fields=name,first_name,last_name,email');
		$fb_response_pictur=fb->get('/mepicture?redirect=false&height=200');

		$fb_user=$fb_response->getGraphUser();
		$picture=$fb_response_picture->getGraphUser();

		$_SESSION['fb_user_id']=$fbuser->getProperty('id');
		$_SESSION['fb_user_name']=$fbuser->getProperty('name');
		$_SESSION['fb_user_email']=$fbuser->getProperty('email');
		$_SESSION['fb_user_pic']=$picture['url'];
	}
	catch(Facebook\Exceptions\FacebookResponseException $e) 
	{
		echo'Facebook API Error: ' . $e->getMessage;
		session_destroy();
		header("Location:index.php");
		exit;
	}
	catch(Facebook\Exceptions\FacebookResponseException $e)
	{
		echo'Facebook SDK Error: ' . $e->getMessage;
	}
}

else
{
	$fb_login_url=$fb_helper->getLoginUrl(FB_BASE_URL);
}
?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<meta charset="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Google/Facebook Log in</title>

		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<script>

	function google()
	{

	window.location = "<?php echo $loginURL ?>";

	}

</script>
</head>
<body>
	<br>
<div class="date" style="float: right; margin-right: 230px;">
		<?php
		$date = date('M/d/y');
		echo 'Date: ', $date;
		?>
		<br>
		<?php
		date_default_timezone_set("Singapore");
		$time = date('h:i:s a');
		echo 'Time: ', $time;
		?>
</div>

	<div class="name" style="float: left; margin-left: 230px;">
		Louie Catabay
		<br>
		BSIT3C
		<br>
		Web Systems and Technology 2
	</div>
<br><br><br><br><br>

		<div class="container">
		<h3 style="text-align: center;">Log in using google or facebook</h3>
			<div class="row justify-content-center">
				<div align="center">

		<form>
			<img src="glogo.png" width="65px" height="35px">
			<input type="button" onclick="google()" value="Log in with google" class="btn btn-danger">

			<img src="flogo.png" width="40px" height="40px">
			<a href="<?php echo $fb_login_url ?>"><input type="button" value="Log in with facebook" class="btn btn-primary"></a>
		</form>

				</div>	
			</div>
		</div>



</body>
</html>