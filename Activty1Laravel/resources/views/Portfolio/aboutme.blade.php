
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <title>About me</title>
  <style >
body{
  background-color: #94B49F;
 /*Navigation bar*/
}
.navbar{
  display: flex;
  justify-content: left;
  align-items: left;
  position: sticky;
  top: 0;
}
.navbar ul{
  display: flex;
  list-style: none;
  margin: 10px 0px;
}
.navbar ul li{
  font-family: century;
  font-size: 1.1rem;
  font-weight: bold;
}
.navbar ul li a{
  text-decoration: none;
  color: black;
  padding: 8px 25px;
  transition: all .5s ease;
}
.navbar ul li a:hover{
  background-color:#B4CFB0 ;
  color: black;
  box-shadow: 0 0 10px #008169;
}
.active{
    background-color:#B4CFB0 ;
  color: black;
  box-shadow: 0 0 10px #008169;
}
.footer {
  position: fixed;
  padding: 2vh;
  left: 0;
  bottom: 0;
  width: 100%;
  height: auto;
  background-color: #789395;
  color: white;
  text-align: left;
}
.avatar {
  vertical-align: middle;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  float: left;
  margin-left: 8vh;
  margin-top: -.5vh;
}
.hobby img{
  float: left;
  height: 30vh;
  width: 30vh;
  margin-left: 2vh;
}
.hobby{
  margin-left: 62vh;
  margin-top: -35vh;
}
.acheivment img{
  float: left;
  height: 30vh;
  width: 30vh;
  margin-left: 2vh;
}
.acheivment{
  margin-left: 62vh;
  margin-top: -35vh;
  margin-bottom: 50vh;
}
.navbar ul li{
  margin-left: 5px;
}

  </style>
</head>

<body>

<nav class="navbar" style="background-color: #E5E3C9;">
    <ul>
      <li><a  href="/Portfolio/index">Home</a></li>
      <li class="active"><a  href="/Portfolio/aboutme">About me</a></li>
      <li><a  href="/Portfolio/register">Register</a></li>
      <li><a  href="/Portfolio/login">Log in</a></li>
    </ul>
</nav>
<br>
<h3 style="text-align:center; font-family: helvetica; font-weight: bold;">About me</h3>
<br><br>
<div class="self">
  <img src="/imgs/dp.jpg" alt="Avatar" class="avatar">
    <h2 style="margin-left:15vh; margin-top: -9.5vh; color: white;">Louie L. Catabay</h2>
</div>


<div style="border:2px none;
    position:absolute;
    left:10vh;
    margin-top: 2.5vh;
    color: white;
    z-index:99;
    font-weight: bold;
    font-size: 2vh;
    background-color: #94B49F;">Hobby</div>

<div class="name">
<p style="text-align: justify; width: 55vh; height: 30vh; margin: 5vh; color: white; border: ridge; padding:2vh">

I like watching or reading anime or movies, biking, and playing video games. I love watching anime because the author can express anything he/she wants that he/she is creating. When I am biking I don't bike alone because I don't have the tools if my bike got damaged. I like playing video games but I get frustrated often if my teammates are troll or AFK during a match.</p>

<div class="hobby">
  <img src="/imgs/watching.gif">
  <img src="/imgs/biking.gif">
  <img src="/imgs/rage.gif">
</div>


<br><br>
<br><br><br>
<br><br><br>
<br><br><br>

<div style="border:2px none;
    position:absolute;
    left:10vh;
    margin-top:3.5vh;
    color: white;
    z-index:99;
    font-weight: bold;
    font-size: 2vh;
    background-color: #94B49F;">Achievment</div>
<div class="name">
<p style="text-align: justify; width: 55vh; height: 30vh; margin: 5vh; color: white; border: ridge; padding:2vh">
I don't have that much achievement at my current age which is 21 but these are some that I am proud of;
<br><br>I know a little bit of programming
<br>I am a manager in Axie Infinity
<br>I have a NC II Certificate
<br>I can swim
<br><br>
</p>

<div class="acheivment">
  <img src="/imgs/programming.gif">
  <img src="/imgs/axie.gif">
  <img src="/imgs/swimming.gif">
</div>


<footer class="footer">
  Contact me @ 
  <a href="#"><img src="/imgs/gmail.png"style="  margin-left: 2vh; width: 40px; height: 40px; border-radius: 50%;"></a>
  <a href="#"><img src="/imgs/fb.png"style=" margin-left: 2vh; width: 40px; height: 40px; border-radius: 50%;"></a>
  <a href="#"><img src="/imgs/instagram.png"style=" margin-left: 2vh; width: 40px; height: 40px; border-radius: 50%;"></a>


  
</footer>

</body>
</html>