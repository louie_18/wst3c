<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>
* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 600px;
  position: relative;
  float: right;
  margin-right: 20vh;
  margin-top: -32vh;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 25px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
  font-weight: bold;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}


/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>

  <style >
body{
  background-color: #94B49F;

 /*Navigation bar*/
}
.navbar{
  display: flex;
  justify-content: left;
  align-items: left;
  position: sticky;
  top: 0;
}
.navbar ul{
  display: flex;
  list-style: none;
  margin:10px 0px;
}
.navbar ul li{
  font-family: century;
  font-size: 1.1rem;
  font-weight: bold;
}
.navbar ul li a{
  text-decoration: none;
  color: black;
  padding: 8px 25px;
  transition: all .5s ease;
}
.navbar ul li a:hover{
  background-color:#B4CFB0 ;
  color: black;
  box-shadow: 0 0 10px #008169;
}
.active{
    background-color:#B4CFB0 ;
  color: black;
  box-shadow: 0 0 10px #008169;
}
.self h2{
font-weight: bold;
font-family: georgia;
}
.self p{
  color: #E5E3C9;
  font-family: helvetica;
}
.self{
  float: left;
  margin-left: 5vh;
}
.self img{
margin-left: 5vh;
}
.recommend{
  float: right;
  margin-top: -16vh;
  margin-right: 9vh;
  font-family: helvetica;
  font-weight: bold;
}
.button a{
 color: white;
 font-weight: bold;
 text-decoration: none;
}
.button{
  float: left;
  margin-left: -35vh;
  padding: 10px;
  border-radius:10%;
  box-shadow: 5px 5px #789395;
}
.avatar {
  vertical-align: middle;
  width: 150px;
  height: 150px;
  border-radius: 50%;
}
.navbar ul li{
  margin-left: 5px;
}
</style>


<title>Home</title>
</head>
<body>
<nav class="navbar" style="background-color: #E5E3C9;">
    <ul>
      <li class="active"><a  href="/Portfolio/index">Home</a></li>
      <li><a  href="/Portfolio/aboutme">About me</a></li>
      <li><a  href="/Portfolio/register">Register</a></li>
      <li><a  href="/Portfolio/login">Log in</a></li>
    </ul>
</nav>

<br><br><br><br>

<div class="self">
  <img src="/imgs/dp.jpg" alt="Avatar" class="avatar">
    <h5>Hello, my name is</h5>
    <h2>Louie L. Catabay</h2>
    <h3>And I Love <font color="white"><strong>Anime</strong></font></h3>
</div>

<br><br>

<div>
  <h1 class="recommend">RECOMMENDED <font color="white"><strong>MANGA</strong></font> AND <font color="white"><strong>ANIME</strong></font></h1>
</div>

<br><br><br><br><br><br><br><br><br>

<button class="button" style="background-color:#789395; border-color:#94B49F; color:white" >  <a href="/Portfolio/aboutme">Know more about me</a></button>
<div class="slideshow-container">

<div class="mySlides fade">
  <div class="numbertext">1 / 8</div>
  <img src="/imgs/bclover.jpg" height = "540px" style="width:540px">
  <div class="text">Black Clover</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">2 / 8</div>
  <img src="/imgs/eleceed.jpg"height = "540px" style="width:540px">
  <div class="text">Eleceed</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">3 / 8</div>
  <img src="/imgs/nbleese.jpg" height = "540px"style="width:540px">
  <div class="text">Nobleese</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">4 / 8</div>
  <img src="/imgs/gofhighschool.jpg" height = "540px"style="width:540px">
  <div class="text">God of highschool</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">5 / 8</div>
  <img src="/imgs/tghoul.jpg" height = "540px"style="width:540px">
  <div class="text">Tokyo Ghoul</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">6 / 8</div>
  <img src="/imgs/gofthefireflieswebp.webp" height = "540px"style="width:540px">
  <div class="text">Grave of the fireflies</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">7 / 8</div>
  <img src="/imgs/totoro.webp" height = "540px"style="width:540px">
  <div class="text">My Neighbor Totoro</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">8 / 8</div>
  <img src="/imgs/demonslayer.jpg" height = "540px"style="width:540px">
  <div class="text">Demon Slayer</div>
</div>


</div>
<br>


<script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}
  slides[slideIndex-1].style.display = "block";
  setTimeout(showSlides, 3000); // Change image every 3 seconds
}
</script>

</body>
</html> 
