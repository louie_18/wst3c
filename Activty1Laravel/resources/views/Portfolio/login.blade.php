
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <title>Log in</title>
  <style >
body{
  background-color: #94B49F;
 /*Navigation bar*/
}
.navbar{
  display: flex;
  justify-content: left;
  align-items: left;
  position: sticky;
  top: 0;
}
.navbar ul{
  display: flex;
  list-style: none;
  margin: 10px 0px;
}
.navbar ul li{
  font-family: century;
  font-size: 1.1rem;
  font-weight: bold;
}
.navbar ul li a{
  text-decoration: none;
  color: black;
  padding: 8px 25px;
  transition: all .5s ease;
}
.navbar ul li a:hover{
  background-color:#B4CFB0 ;
  color: black;
  box-shadow: 0 0 10px #008169;
}
.active{
    background-color:#B4CFB0 ;
  color: black;
  box-shadow: 0 0 10px #008169;
}

.navbar ul li{
  margin-left: 5px;
}
/*form*/
form {
  border: 1px solid black;
  align-items: center;
  padding: 10px;
}
.login
{
  margin: 100px;
  border:2px solid;
    position:relative;
    padding:20px;
    margin:150px;
    margin-top:30px;
}

  </style>

  <script>
function login() {
  window.location.assign("/Portfolio/aboutme")
}
function register() {
  window.location.assign("/Portfolio/register")
}
</script>
</head>

<body>

<nav class="navbar" style="background-color: #E5E3C9;">
    <ul>
      <li><a  href="/Portfolio/index">Home</a></li>
      <li><a  href="/Portfolio/aboutme">About me</a></li>
      <li><a  href="/Portfolio/register">Register</a></li>
      <li class="active"><a  href="/Portfolio/login">Log in</a></li>
    </ul>
</nav>
<br>
<div style="border:2px none;
    position:absolute;
    left:25vh;
    margin-top: 5vh;
    z-index:99;
    font-weight: bold;
    font-size: 2vh;
    background-color: #94B49F;">Log in</div>
<div class="login">
  <form>
  <label for="uname">Usename:</label>
  <br>
  <input type="text" id="uname" placeholder="Username"required >
  <br>
  <label for="pass">Password:</label>
  <br>
  <input type="password" id="pass" placeholder="Password"  required>
  <br>
  <input type="button" onclick="login()" value="Log in" style="background-color:#789395; border-color:#94B49F; padding: 10px;
  border-radius:10%; box-shadow: 3px 3px #789395; margin-top: 1vh; color: white; font-weight: bold;">

  <input type="button" onclick="register()" value="Register" style="background-color:#407A54; border-color:#407A54; padding: 10px;
  border-radius:10%; box-shadow: 3px 3px #407A54; color: white; font-weight: bold;">

  </form>
  
</div>

</body>
</html>