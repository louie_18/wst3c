<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Portfolio/aboutme', function () {
    return view('Portfolio/aboutme');
});

Route::get('/Portfolio/login', function () {
    return view('Portfolio/login');
});

Route::get('/Portfolio/index', function () {
    return view('Portfolio/index');
});

Route::get('/Portfolio/register', function () {
    return view('Portfolio/register');
});
