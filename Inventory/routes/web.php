<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//login
Route::get('/login', [UserController::class,'login'])->name('login');
Route::get('/register', [UserController::class,'register']);
Route::post('/register', [UserController::class,'registeruser'])->name('registeruser');
Route::post('/loginuser/products', [UserController::class,'loginuser'])->name('loginuser');
Route::get('/loginuser/products', [UserController::class,'loggedinuser']);


  
Route::resource('products', ProductController::class);
