
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>
    <title>Items</title>
<style>
    body
    {
        background-image: url('/logindsgn/mainbg.png');
        background-size: cover;
    }

    .navbar{
      display: flex;
      justify-content: right;
      align-items: right;
      position: sticky;
      top: 0;
      background-image: url('/logindsgn/mainbg.png');
      background-size: 100% 80%;
      background-repeat: no-repeat;
    }
    .navbar ul{
      display: flex;
      list-style: none;
      margin:20px 0px;
      margin-right: 8px;
    }
    .navbar ul li{
      font-family: century;
      font-size: 1.5rem;
      font-weight: bold;
    }
    .navbar ul li a{
      text-decoration: none;
      color: #666666;
      padding: 8px 25px;
      transition: all .5s ease;
    }
    .navbar ul li a:hover{
      background-color:#f48b6f;
      color: white;
      box-shadow: 0 0 10px #f48b6f;
      margin: 10px;
      border-radius: 10%;
    }
    .navbar li img
    {
        border-radius: 50%;
        width: 85px;
        height: 80px;
        float: left;
        margin-top: -20px;
        margin-left: -545px;

    }
    .navbar a.active
    {
        background-color:#f48b6f;
        color: white;
    } 
    * {
  box-sizing: border-box;
}

#myInput {
  background-image: url('/logindsgn/searchicon.png');
  background-size: 30px;
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}


#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 18px;
}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1; 
}
th {
  cursor: pointer;
}
</style>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc"; 
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;      
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>

</head>
<body>
<nav class="navbar">
    <ul>
      <li><img src="/logindsgn/logo.jpg"></li>
      <li><a  class="active" href="{{ route('products.index') }}">Items</a>
      <li><a  href="{{ route('feedbacks.index') }}">Feedbacks</a></li>
      <li><a  href="{{ route('usage.index') }}">Usage</a></li>
      <li><a  href="{{ route('aboutme.index') }}">About me</a></li>
      <li><a  href="{{ route('login') }}">Logout</a></li>
    </ul>
</nav>

 <h1 style="color: #666666; text-align: center; margin-top: -20px;">Items</h1>

 </div>


@extends('products.layout')

@section('content')
            <div class="card" >
                <div class="card-header">
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('products.create') }}"> Add New Product</a>
                    </div>
<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Items.." title="Type in a name" style="width:50%">
                </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


<table id="myTable" class="sortable table-bordered">
  <tr class="header">
            <th onclick="sortTable(0)">Item</th>
            <th >Image</th>
            <th onclick="sortTable(1)">Details/Prices</th>
            <th >No</th>
            <th width="280px">Action</th>
  </tr>




        @foreach ($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td><img src="/image/{{ $product->image }}" width="100%" height="150px"></td>
            <td>{{ $product->detail }}</td>
            <td>{{ ++$i }}</td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
     
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
      
                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
     
                    @csrf
                    @method('DELETE')
        
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
</table>

    
    {!! $products->links() !!}
        
@endsection
    </div class="card-body">



</body>
</html>
<?php

?>