<?php
  
namespace App\Http\Controllers;
  
use App\Models\Product;
use Illuminate\Http\Request;
  
class ProductController extends Controller
{
    

    public function index()
    {
        $products = Product::latest()->paginate(10000);
    //redirect to index.blade.php
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   

    public function create()
    {
        //returnview to create.blade.php
        return view('products.create');
    }
    

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
    
        Product::create($input);
     //redirect to index.blade.php
        return redirect()->route('products.index')
                        ->with('success','Product added successfully.');
    }
     

    public function show(Product $product)
    {
        //redirect to show.blade.php
        return view('products.show',compact('product'));
    }
     

    public function edit(Product $product)
    {
        //redirect to edit.blade.php
        return view('products.edit',compact('product'));
    }
    

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required'
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        else
        {
            unset($input['image']);
        }
          
        $product->update($input);
    
        return redirect()->route('products.index')
                            ->with('success','Product updated successfully');
    }
  

    public function destroy(Product $product)
    {
        $product->delete();
     //redirect to index.blade.php
        return redirect()->route('products.index')
                            ->with('success','Product deleted successfully');
    }

}