<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="icon" href="/assets/fav.png" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
	<link href="/css/A_login.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
	@stack('css-internal')
	@stack('css-external')
	<title>Admin Log In</title>

	<script type="text/javascript">
		$(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
	</script>

</head>
<body>
	<div class="container-fluid">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
			    <ul>
			        @foreach ($errors->all() as $error)
			            <li>{{ $error }}</li>
			        @endforeach
			       	</ul>
			   	</div>
		@endif
	</div>
	<div class="main-wrapper pb-4">
		<form action="" method="POST">
		    {{ csrf_field() }}
		    <div class="form-cont-1">
		        <h1>Sign In</h1>
		    </div>
		        
		    <div class="form-cont-2">
		      <p>Email address or username</p>
		      <input type="text" name="username" placeholder="Email address or username" />
		    </div>
		    <div class="form-cont-2">
		      <div class="pass-cont">
		        <p>Password</p>
		        <input type="password" name="password" placeholder="Password" />
		      </div>
		    </div>
		    <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4 mt-4">
	            <button type="submit" class="btn btn-primary btn-md mb-4">Sign In</button>
	        </div>
		</form>
	    <div class="float circle c-1"></div>
	    <div class="float circle c-2"></div>
	    <div class="float circle c-3"></div>
	    <div class="float line l-1"></div>
	    <div class="float line l-2"></div>
	    <div class="float line l-3"></div>
	</div>
	@include('sweetalert::alert')
	@stack('javascript-external')
	@stack('javascript-internal')
</body>
</html>