<div class="container mx-auto mt-4">
    <div class="card">
    <h5 class="card-header" style="background-color: #FFAB91;"><b>Your certificate is valid.</b></h5>
        <div class="card-body">

            <h5 class="card-title"><b>Token:</b> {{$details['token']}}</h5>
            <h5 class="card-title mt-4"><b>Participant Profile</b></h5>
            <p class="card-text"><b>Name:</b> {{$details['awardee']}}</p>
            <p class="card-text"><b>Seminar:</b> {{$details['seminar']}}</p>
            <p class="card-text"><b>Venue:</b> {{$details['venue']}}</p>
            <p class="card-text"><b>Date:</b> {{$details['sdate']}}
                @if ($details['sdate'] != $details['edate'])
                to {{$details['edate']}}
                @endif
            </p>
                
        </div>
    
  </div>
  </div>