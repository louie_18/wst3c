<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BGTemplates extends Model
{
    use HasFactory;
    protected $table = "bg_templates";
    protected $fillable = [
        'temp_name',
        'imgpath'
    ];
}
