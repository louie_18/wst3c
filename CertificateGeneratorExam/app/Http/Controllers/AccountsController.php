<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class AccountsController extends Controller
{
    public function loginAdminForm() {
      return view('/admin');
    }

    public function validateAdminForm(Request $request) {
      $this->validate($request,[
         'username'=>'required|min:7',
         'password'=>'required|max:10|min:7'
        ]);

        $username = $request->input('username');
        $password = md5($request->input('password'));

        $admin = DB::select("SELECT * FROM admin WHERE (email = ? OR username = ?) AND password = ?", [$username, $username, $password]);
        
        if($admin){
            $data = $request->input();
            $request->session()->put('username',$data['username']);
            foreach($admin as $admin1){
              $id = $admin1->admin_id;
              $email = $admin1->email;
            }
            
            $request->session()->put('username',$data['username']);
            $request->session()->put('id',$id);
            $request->session()->put('email',$email);
            return redirect('/generator');
        }
        else{
         return redirect('admin')->withErrors("Unsuccessful login. Sorry.");
        }
    }
}
