<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Message;
use Session;
use Hash;
use Illuminate\Support\Facades\DB;
class MessageController extends Controller
{
    function addmessage(Request $request)
    {
        $request->validate([
            'message'=>'required|min:8',
        ]);
          $add=DB::table('messages')->insert([
                'messages'=>$request->message
            ]);
        if ($add)
        {
           return back()->with('success','Message Sent');
        }
        else
        {

           return back()->with('error','Message Not Sent');
        }
    }

    function showmessage(Request $request)
    {
        $messages=DB::table('messages')->get();
        return view('messages.index')->with('messages',$messages);
    }
}
