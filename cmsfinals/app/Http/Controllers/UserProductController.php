<?php
  
namespace App\Http\Controllers;
  
use App\Models\Product;
use App\Models\feedbacks;
use App\Models\aboutmes;
use App\Models\usages;
use Illuminate\Http\Request;
use DB;
class UserProductController extends Controller
{
    

    public function showitem()
    {
        $items=DB::table('products')->get();

        return view('healingcharms.items')->with('items',$items);
    }
    public function showfeedbacks()
    {
        $feedbacks=DB::table('feedbacks')->get();

        return view('healingcharms.feedbacks')->with('feedbacks',$feedbacks);
    }
    public function showusage()
    {
        $usages=DB::table('usages')->get();

        return view('healingcharms.usages')->with('usages',$usages);
    }
    public function showaboutme()
    {
        $aboutme=DB::table('aboutmes')->get();

        return view('healingcharms.aboutme')->with('aboutme',$aboutme);
    }
   

}