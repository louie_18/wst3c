<?php
  
namespace App\Http\Controllers;
  
use App\Models\usages;
use Illuminate\Http\Request;
  
class UsageController extends Controller
{

    public function index()
    {
        $usages = usages::latest()->paginate(10000);
    //redirect to index.blade.php
        return view('usage.index',compact('usages'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   

    public function create()
    {
        //returnview to create.blade.php
        return view('usage.create');
    }
    

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
    
        usages::create($input);
     //redirect to index.blade.php
        return redirect()->route('usage.index')
                        ->with('success','Product added successfully.');
    }
     

    public function show(usages $usage)
    {
        //redirect to show.blade.php
        return view('usage.show',compact('usage'));
    }
     

    public function edit(usages $usage)
    {
        //redirect to edit.blade.php
        return view('usage.edit',compact('usage'));
    }
    

    public function update(Request $request, usages $usage)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required'
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        else
        {
            unset($input['image']);
        }
          
        $usage->update($input);
    
        return redirect()->route('usage.index')
                            ->with('success','Product updated successfully');
    }
  

    public function destroy(usages $usage)
    {
        $usage->delete();
     //redirect to index.blade.php
        return redirect()->route('usage.index')
                            ->with('success','Product deleted successfully');
    }
}