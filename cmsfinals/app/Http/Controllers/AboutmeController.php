<?php
  
namespace App\Http\Controllers;
  
use App\Models\aboutmes;
use Illuminate\Http\Request;
  
class AboutmeController extends Controller
{

    public function index()
    {
        $aboutmes = aboutmes::latest()->paginate(100);
    //redirect to index.blade.php
        return view('aboutme.index',compact('aboutmes'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   

    public function create()
    {
        //returnview to create.blade.php
        return view('aboutme.create');
    }
    

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
    
        aboutmes::create($input);
     //redirect to index.blade.php
        return redirect()->route('aboutme.index')
                        ->with('success','Feedback added successfully.');
    }
     

    public function show(aboutmes $aboutme)
    {
        //redirect to show.blade.php
        return view('aboutme.show',compact('aboutme'));
    }
     

    public function edit(aboutmes $aboutme)
    {
        //redirect to edit.blade.php
        return view('aboutme.edit',compact('aboutme'));
    }
    

    public function update(Request $request, aboutmes $aboutme)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required'
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        else
        {
            unset($input['image']);
        }
          
        $aboutme->update($input);
    
        return redirect()->route('aboutme.index')
                            ->with('success','Product updated successfully');
    }
  

    public function destroy(aboutmes $aboutme)
    {
        $aboutme->delete();
     //redirect to index.blade.php
        return redirect()->route('aboutme.index')
                            ->with('success','Information deleted successfully');
    }
}