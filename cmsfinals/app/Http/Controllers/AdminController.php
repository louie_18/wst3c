<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\admins;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use Hash;
class AdminController extends Controller
{
    
    function login()
    {
        return  view ('login.index');
    }


     function checklogin(Request $request)
    {
        $this->validate($request,[
            'username'=>'required',
            'password'=>'required|min:3'
        ]);
        $admin=DB::table('admins')->where('username',$request->username)->where('password',$request->password)->first();

        if ($admin) 
        {
            //$request->session()->put('loginID', $admin->id);
            return redirect('admin/products');
        }
        else 
        {
           return back()->with('error','Invalid Login Credentials');
        }
    }

    function addadmin(Request $request)
    {
        $request->validate([
            'username'=>'required|min:3|max:12',
            'password'=>'required|min:8|max:12'
        ]);

        $user = new admins();
        $user->username=$request->username;
        $user->password=$request->password;
        $res=$user->save();

        if ($res) 
        {
            // code...
            return back()->with('success','ADMIN ADDED');
        }
        else 
        {
            // code...
            return back()->with('fail','ADMIN NOT ADDED');
        }
    }
    function addadmins()
    {
        return  view ('addadmins.index');
    }

}
