<?php
  
namespace App\Http\Controllers;
  
use App\Models\feedbacks;
use Illuminate\Http\Request;
  
class FeedbackController extends Controller
{

    public function index()
    {
        $feedbacks = feedbacks::latest()->paginate(10000);
    //redirect to index.blade.php
        return view('feedbacks.index',compact('feedbacks'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function create()
    {
        //returnview to create.blade.php
        return view('feedbacks.create');
    }
    

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        //$items=DB::table('products')->get();

        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
    
        feedbacks::create($input);
     //redirect to index.blade.php
        return redirect()->route('feedbacks.index')
                        ->with('success','Feedback added successfully.');
    }
     

    public function show(feedbacks $feedback)
    {
        //redirect to show.blade.php
        return view('feedbacks.show',compact('feedback'));
    }
     

    public function edit(feedbacks $feedback)
    {
        //redirect to edit.blade.php
        return view('feedbacks.edit',compact('feedback'));
    }
    

    public function update(Request $request, feedbacks $feedback)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required'
        ]);
  
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        else
        {
            unset($input['image']);
        }
          
        $feedback->update($input);
    
        return redirect()->route('feedbacks.index')
                            ->with('success','Product updated successfully');
    }
  

    public function destroy(feedbacks $feedback)
    {
        $feedback->delete();
     //redirect to index.blade.php
        return redirect()->route('feedbacks.index')
                            ->with('success','Product deleted successfully');
    }


}