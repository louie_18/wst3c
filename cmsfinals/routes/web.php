<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UsageController;
use App\Http\Controllers\AboutmeController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\UserProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//admin
Route::get('/', function () {
    // Only authenticated users may enter...
    return view('login.index');
})->name('/');

// Route::group(['middleware'=>'App\Http\Middleware\Authenticate'],function(){
// Route::resource('admin/products', ProductController::class);
// Route::resource('admin/feedbacks', FeedbackController::class);
// Route::resource('admin/usage', UsageController::class);
// Route::resource('admin/aboutme', AboutmeController::class);
// });

//admin
Route::resource('admin/products', ProductController::class);
Route::resource('admin/feedbacks', FeedbackController::class);
Route::resource('admin/usage', UsageController::class);
Route::resource('admin/aboutme', AboutmeController::class);
//messages
Route::post('healingcharms/aboutme', [MessageController::class,'addmessage'])->name('addmessage');
Route::get('admin/messages', [MessageController::class,'showmessage'])->name('showmessage');

//add admin
Route::post('admin/addAdmin', [AdminController::class,'addadmin'])->name('addadmin');
Route::get('admin/addAdmin', [AdminController::class,'addadmins'])->name('addadmins');

Route::get('/', [AdminController::class,'login'])->name('login');
Route::post('/', [AdminController::class,'checklogin'])->name('checklogin');

//users
Route::get('healingcharms/items', [UserProductController::class,'showitem'])->name('healingcharmsitem');
Route::get('healingcharms/feedbacks', [UserProductController::class,'showfeedbacks'])->name('healingcharmsfeedbacks');
Route::get('healingcharms/usage', [UserProductController::class,'showusage'])->name('healingcharmsusage');
Route::get('healingcharms/aboutme', [UserProductController::class,'showaboutme'])->name('healingcharmsaboutme');



