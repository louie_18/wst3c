<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="/logindsgn/logo.jpg" sizes="16x16">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <title>Healing Charms</title>
<style>
    body
    {
        background-image: url('/logindsgn/mainbg.png');
        background-size: cover;
      font-family: puppet;
    }
    .contents
    {
        padding-left: 50px;
        padding-right: 50px;

    }
    .navbar{
      display: flex;
      justify-content: right;
      align-items: right;
      position: sticky;
      top: 0;
      background-image: url('/logindsgn/mainbg.png');
      background-size: 100% 80%;
      background-repeat: no-repeat;
    }
    .navbar ul{
      display: flex;
      list-style: none;
      margin:20px 0px;
      margin-right: 8px;
    }
    .navbar ul li{
      font-size: 1.5rem;
      font-weight: bold;
    }
    .navbar ul li a{
      text-decoration: none;
      color: #666666;
      padding: 8px 25px;
      transition: all .5s ease;
    }
    .navbar ul li a:hover{
      background-color:#f48b6f;
      color: white;
      box-shadow: 0 0 10px #f48b6f;
      margin: 10px;
      border-radius: 10%;
    }
    .navbar li img
    {
        border-radius: 50%;
        width: 85px;
        height: 80px;
        float: left;
        margin-top: -20px;
    }
    .logo li
    {
        color: #666666;
        margin-left: -90vh;
    }
    .navbar a.active
    {
        background-color:#f48b6f;
        color: white;
    }  
    .item img
    {
        width: 100%;
        height: 200px;
        align-items: center;
    }
 .item {
  border: 3px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 10px; /* Some padding */
}
/* Add a hover effect (blue shadow) */
.item:hover {
  box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
.footer a
{
    text-decoration: none;
}
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-image: url('/logindsgn/mainbg.png');
  color: white;
  font-size: 15px;
  padding: 10px;
  font-weight: bold;
  text-align: center;
}
</style>

</head>
<body>

<nav class="navbar">
    <ul>
        <div class="logo">
      <li><img src="/logindsgn/logo.jpg">Healing Charms PH</li>
        </div>
      <li><a  href="{{ route('healingcharmsitem') }}">Items</a>
      <li><a  href="{{ route('healingcharmsfeedbacks') }}">Feedbacks</a></li>
      <li><a  class="active" href="{{ route('healingcharmsusage') }}">Usage</a></li>
      <li><a  href="{{ route('healingcharmsaboutme') }}">About me</a></li>
    </ul>
</nav>
 <h1 style="color: #666666; text-align: center; margin-top: -20px; margin-bottom: 30px;">Usage</h1>

<div class="contents">

    
<div class="row">
@foreach($usages as $usages)

    <div class="col-lg-3">

        <div class="item mb-5">
        <!-- <img src="{{ URL('/image/',$usages->image) }}"> --> 
              <a target="_blank" href="/image/{{ $usages->image }}"><img class="img-fluid"  src="/image/{{ $usages->image }}" alt="..."></a>
            <h4 style="color: #666666; ">Item: <strong>{{ $usages->name }}</strong></h4>
            <p style="color: #666666; text-align: justify;"><strong>Usage: </strong>{{ $usages->detail }}</p>

        </div>


    </div>

@endforeach
</div>

</div>

<br><br><br><br><br>
<div class="footer">
<a href="https://shopee.ph/healingcharmsph" target="_blank">
    <img src="/logindsgn/shopee.jpg" style="border-radius: 50%; height: 50px; width: 50px; margin-left: 5px;">
</a>

<a href="https://www.lazada.com.ph/shop/healing-charms-ph?dsource=share&laz_share_info=240878775_100_1600_0_215741333_null&laz_token=46cf188fc8ac676dc015f4e59215e76f&laz_trackid=2:mm_150840658_51801640_2010951626:clkgg2p351g5baph45i0of&mkttid=clkgg2p351g5baph45i0of" target="_blank">
    <img src="/logindsgn/lazada.png" style="border-radius: 50%; height: 50px; width: 50px; margin-left: 5px;">
</a>
    <div style="color: #666666; text-align: center;">
    © 2022 Louie Catabay. All Rights Reserved.
    </div>

</div>
</body>
</html>