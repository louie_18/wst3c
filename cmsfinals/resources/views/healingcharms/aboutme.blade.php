<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="/logindsgn/logo.jpg" sizes="16x16">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
    <title>Healing Charms</title>
<style>
    body
    {
        background-image: url('/logindsgn/mainbg.png');
        background-size: cover;
        font-family: puppet;
    }
    .contents
    {
        padding-left: 50px;
        padding-right: 50px;
        margin-left: 60px;
    }
    .navbar{
      display: flex;
      justify-content: right;
      align-items: right;
      position: sticky;
      top: 0;
      background-image: url('/logindsgn/mainbg.png');
      background-size: 100% 80%;
      background-repeat: no-repeat;
    }
    .navbar ul{
      display: flex;
      list-style: none;
      margin:20px 0px;
      margin-right: 8px;
    }
    .navbar ul li{
      font-family: century;
      font-size: 1.5rem;
      font-weight: bold;
    }
    .navbar ul li a{
      text-decoration: none;
      color: #666666;
      padding: 8px 25px;
      transition: all .5s ease;
    }
    .navbar ul li a:hover{
      background-color:#f48b6f;
      color: white;
      box-shadow: 0 0 10px #f48b6f;
      margin: 10px;
      border-radius: 10%;
    }
    .navbar li img
    {
        border-radius: 50%;
        width: 85px;
        height: 80px;
        float: left;
        margin-top: -20px;
    }
    .navbar a.active
    {
        background-color:#f48b6f;
        color: white;
    }  
    .item img
    {
        width: 450px;
        height: 450px;
    }

.row
{
border-radius: 4px;
border: 2px solid #ddd; /* Gray border */
padding: 20px;
margin-bottom: 30px;
}
.row:hover {
  box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}

.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-image: url('/logindsgn/mainbg.png');
  color: white;
  font-size: 15px;
  padding: 10px;
  font-weight: bold;
  text-align: center;
}
.footer a
{
    text-decoration: none;
}
    .logo li
    {
        color: #666666;
        margin-left: -87vh;
    }
</style>

</head>
<body>

<nav class="navbar">
    <ul>
        <div class="logo">
      <li><img src="/logindsgn/logo.jpg">Healing Charms PH</li>
        </div>
      <li><a  href="{{ route('healingcharmsitem') }}">Items</a>
      <li><a  href="{{ route('healingcharmsfeedbacks') }}">Feedbacks</a></li>
      <li><a  href="{{ route('healingcharmsusage') }}">Usage</a></li>
      <li><a  class="active" href="{{ route('healingcharmsaboutme') }}">About me</a></li>
    </ul>
</nav>
 <h1 style="color: #666666; text-align: center; margin-top: -20px; margin-bottom: 30px;">About me</h1>

<div class="contents">

    

@foreach($aboutme as $aboutme)
<div class="row">

    <div class="col-lg-5">
        <div class="item">
        <img src="{{ URL('/image/',$aboutme->image) }}">
        </div>
    </div>

    <div class="col-lg-7">
        <h4 style="color: #666666;">Hi this is: <strong>{{ $aboutme->name }}</strong></h4>
        <p style="color: #666666; text-align: justify;"><strong>Our goal is to:</strong> {{ $aboutme->detail }}</p>
    </div>
</div>
@endforeach

<div data-aos="fade-up">
        <iframe style="border:0; width: 100%; height: 350px;" 
        src="https://maps.google.com/maps?q=%20Sta%20Lucia%20complex%20in%20Dolores%20Quezon%20in%20the%20foot%20hills%20of%20Mt.%20Banahaw&t=k&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" allowfullscreen>
        </iframe>
</div>
<br><br>

     @if ($message = Session::get('error'))
     <div class="alert alert-danger alert-block">
        {{$message}}
         
     </div>
     @endif

     @if (count($errors)>0)
     <div class="alert alert-danger">
         <ul>
             @foreach($errors->all() as $error)
             <li>{{$error}}</li>
             @endforeach
         </ul>
     </div>
     @endif

<form action="{{ route('addmessage') }}" method="POST" enctype="multipart/form-data">
                @if(Session::has('success'))
                    <div class="alert alert-success">{{Session::get('success')}}</div>
                    @endif

                    @if(Session::has('fail'))
                    <div class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
    @csrf
    
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Inquire:</strong>
                <textarea class="form-control" style="height:150px" name="message" placeholder="Inquire"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-success" style="margin-top: 10px;">Inquire</button>
        </div>
    </div>

</form>

</div>
<br><br><br>
<div class="footer">
<a href="https://shopee.ph/healingcharmsph" target="_blank">
    <img src="/logindsgn/shopee.jpg" style="border-radius: 50%; height: 50px; width: 50px; margin-left: 5px;">
</a>

<a href="https://www.lazada.com.ph/shop/healing-charms-ph?dsource=share&laz_share_info=240878775_100_1600_0_215741333_null&laz_token=46cf188fc8ac676dc015f4e59215e76f&laz_trackid=2:mm_150840658_51801640_2010951626:clkgg2p351g5baph45i0of&mkttid=clkgg2p351g5baph45i0of" target="_blank">
    <img src="/logindsgn/lazada.png" style="border-radius: 50%; height: 50px; width: 50px; margin-left: 5px;">
</a>
    <div style="color: #666666; text-align: center;">
    © 2022 Louie Catabay. All Rights Reserved.
    </div>

</div>
</body>
</html>
