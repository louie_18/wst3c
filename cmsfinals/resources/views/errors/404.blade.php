<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error</title>
    <style>
        .error
        {
            background-color: #800000;
            text-align: center;
            color: white;
            padding: 20px;
        }
    </style>
</head>
<body>
    <div class="error">
            <h1><strong>ERROR</strong> Finding Webpage</h1>
            <p>The page you are looking for does not exist</p>
    </div>

</body>
</html>