<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="/logindsgn/logo.jpg" sizes="16x16">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
   
            {{-- Datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    {{-- Datatables --}}
    <title>Feedbacks</title>
<style>
    body
    {
        background-image: url('/logindsgn/mainbg.png');
        background-size: cover;
    }

    .navbar{
      display: flex;
      justify-content: right;
      align-items: right;
      position: sticky;
      top: 0;
      background-image: url('/logindsgn/mainbg.png');
      background-size: 100% 80%;
      background-repeat: no-repeat;
    }
    .navbar ul{
      display: flex;
      list-style: none;
      margin:20px 0px;
      margin-right: 8px;
    }
    .navbar ul li{
      font-family: century;
      font-size: 1.5rem;
      font-weight: bold;
    }
    .navbar ul li a{
      text-decoration: none;
      color: #666666;
      padding: 8px 25px;
      transition: all .5s ease;
    }
    .navbar ul li a:hover{
      background-color:#f48b6f;
      color: white;
      box-shadow: 0 0 10px #f48b6f;
      margin: 10px;
      border-radius: 10%;
    }
    .navbar li img
    {
        border-radius: 50%;
        width: 85px;
        height: 80px;
        float: left;
        margin-top: -20px;
        margin-left: -545px;

    }
    .navbar a.active
    {
        background-color:#f48b6f;
        color: white;
    } 
    * {
  box-sizing: border-box;
}


</style>

    <script>
      $(function(){
            $("#fbTable").DataTable();
        });
    </script>

</head>
<body>

<nav class="navbar">
    <ul>
      <li><img src="/logindsgn/logo.jpg"></li>
      <li><a  href="{{ route('products.index') }}">Items</a>
      <li><a  class="active" href="{{ route('feedbacks.index') }}">Feedbacks</a></li>
      <li><a  href="{{ route('usage.index') }}">Usage</a></li>
      <li><a  href="{{ route('aboutme.index') }}">About me</a></li>
      <li><a  href="{{ route('login') }}">Logout</a></li>
    </ul>
</nav>

 <h1 style="color: #666666; text-align: center; margin-top: -20px;">Feedbacks</h1>
 
 </div>

@extends('feedbacks.layout')
     
@section('content')


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


  <div class="row">
    <div class="col-sm-11 mx-auto">
      <table class="table table-hover w-100 mx-auto" id="fbTable">

        <thead class="table-dark">
          <tr>
            <th width="200px">Item</th>
            <th>Image</th>
            <th>Feedbacks</th>
            <th >No</th>
            <th width="230px"><a style="margin-left: 3vh;" class="btn btn-success" href="{{ route('feedbacks.create') }}"> Add New Feedback</a></th>
          </tr>
        </thead>
        <tbody>
        @foreach ($feedbacks as $feedback)
          <tr>
            <td>{{ $feedback->name }}</td>
            <td>
              <a target="_blank" href="/image/{{ $feedback->image }}"><img class="img-fluid" style="width: 1700px; height: 150px;" src="/image/{{ $feedback->image }}" alt="..."></a>
            </td>
            <td>{{ $feedback->detail }}</td>
            <td>{{ ++$i }}</td>
            <td>
                <form action="{{ route('feedbacks.destroy',$feedback->id) }}" method="POST">
     
                    <a class="btn btn-info" href="{{ route('feedbacks.show',$feedback->id) }}">Show</a>
      
                    <a class="btn btn-primary" href="{{ route('feedbacks.edit',$feedback->id) }}">Edit</a>
     
                    @csrf
                    @method('DELETE')
        
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

    {!! $feedbacks->links() !!}
        
@endsection

</body>
</html>