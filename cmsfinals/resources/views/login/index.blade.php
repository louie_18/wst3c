<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="/logindsgn/logo.jpg"  >
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>

    <style>
    body
    {
        margin: 0;
        padding: 0;
        font-family: helvetica;
        height: 100vh;
        overflow: hidden;
        background-image: url('/logindsgn/bg.png');
        background-repeat: no-repeat;
        background-size: cover;
        /*background: linear-gradient(120deg,#0000,#E17f93);*/
    }
    .center
    {
        position: absolute;
        top:50%;
        left: 38%;
        transform: translate(-50%,-50%);
        width: 500px;
        background-color: white;
        border-radius: 10px;

    }
    .center h1
    {
        text-align: center;
        padding: 20px 0 20px 0;
        border-bottom: 1px solid silver;
        font-weight: bold;
    }
    .center form
    {
        padding: 0 50px;
        box-sizing: border-box;
    }
    form .txt
    {
        position: relative;
        border-bottom: 2px solid silver;
        margin: 30px 0;
    }
    .txt input
    {
        width: 100%;
        padding: 0 5px;
        height: 40px;
        font-size: 15px;
        border: none;
        background: none;
        outline: none;

    }
    .txt label
    {
        position: absolute;
        top: 50%;
        left: 5px;
        color: silver;
        transform: translateY(-50%);
        font-size: 15px;
        pointer-events: none;
    }
    .txt span::before
    {
        content: '';
        position: absolute;
        top: 40px;
        left: 0;
        width: 100%;
        height: 2px;

    }
    .txt input:focus~label,.txt input:valid~label
    {
        top: -7px;
        color: #E17f93;
        font-weight: bold;
    }
    .txt input:focus~span::before,.txt input:valid~span::before
    {
        width: 100%;
    }
    input[type="submit"]
    {
        width: 100%;
        height: 50px;
        margin-bottom: 40px;
        background-color: #f48b6f;
        border-radius: 25px;
        font-size: 20px;
        color: white;
        font-weight: bold;
        outline: none;
        cursor: pointer;
    }
    .warnings
    {
        text-align: center;
        font-weight: bold;
    }
    .warnings ul 
    {
  list-style-type: none;
    }
</style>


    <title>Admin login</title>
</head>

<body>
 <div class="center">
     <h1>Sign in</h1>
     <div class="warnings">
         

     @if ($message = Session::get('error'))
     <div class="alert alert-danger alert-block">
        {{$message}}
         
     </div>
     @endif

     @if (count($errors)>0)
     <div class="alert alert-danger">
         <ul>
             @foreach($errors->all() as $error)
             <li>{{$error}}</li>
             @endforeach
         </ul>
     </div>
     @endif

     </div>
     <form method="POST" action="{{ route('checklogin') }}">
        <div class="txt">
            {{csrf_field()}}
            <input type="text" name="username">
            <label>Username</label>
        </div>
        <div class="txt">
            <input type="password" name="password">
            <label>Password</label>
        </div>
        <input type="submit" value="Login" name="login">
     </form>

 </div>
</body>
</html>