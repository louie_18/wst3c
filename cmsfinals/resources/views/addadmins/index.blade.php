<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="/logindsgn/logo.jpg" sizes="16x16">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <title>Add Admin</title>
<style>
    .container
    {
        margin-top: 50px;
        border-radius: 1%;
        background-color: grey;
        color: white;
        padding: 50px;
        font-family: sans-serif;
        width: 70%;
    }
        .container a
    {
        color: white;
        text-decoration: none;
    }
</style>

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h4>Add Admin</h4>
                <form action="{{ route('addadmin')}}" method="post">

                    @if(Session::has('success'))
                    <div class="alert alert-success">{{Session::get('success')}}</div>
                    @endif

                    @if(Session::has('fail'))
                    <div class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif

                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text"class="form-control" name="username" placeholder="Enter Username" value="{{old('username')}}">
                        <span class="text-danger">@error('username') {{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <label for="passsword">Password</label>
                        <input type="password"class="form-control" name="password" placeholder="Enter Password" value="{{old('passsword')}}">
                        <span class="text-danger">@error('password') {{$message}}@enderror</span>
                    </div>
                    <br>
                    <div class="form-group"> 
                        <button type="submit" class="btn btn-block btn-success">Add Admin</button>
                        <a href="{{ route('aboutme.index')}}" class="btn btn-primary float-end">Back</a>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</html>